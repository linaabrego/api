console.log("Aqui funcionando con Nodemon");
var movimientosJSON=require('./movimientosv2.json')
var jsonQuery =require('json-query')
var requestJson=require('request-json')
var express = require('express')
var bodyparse= require('body-parser')
var app = express()
app.use(bodyparse.json())

app.get('/v1/movimientos',function(req,res)
{
  res.send('Hola API ')
})

app.get('/v1/movimientos',function(req,res) {
    res.sendfile('movimientosv1.json')
})

app.get('/v2/movimientos',function(req,res) {
    res.sendfile('movimientosv2.json')
})

app.get('/v2/movimientos/:id',function(req,res) {
  //console.log(req)
  console.log(req.params.id)
  //console.log(movimientosJSON)
  //res.send("Hemos recibido su peticion de consulta del movimiento: "+req.params.id)
  res.send(movimientosJSON[req.params.id-1])
  //console.log("Hemos recibido su post")
})  //ai pasar parametros a la url

app.get('/v2/movimientosq',function(req,res) {
  console.log(res.query)
  res.send("recibido")
})

app.get('/v2/movimientosp/:id/:nombre',function(req,res) {
  console.log(res.params)
  res.send("recibido")
})


app.post('/v12/movimientos',function(req,res){
  //console.log(req)
  //console.log(req.headers['authorization'])
  //if (req.headers['authorization']!=undefined) {
  var nuevo =req.body
  nuevo.id = movimientosJSON.lenght + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
//else {
  //res.send("No esta autorizado")
//}
})

//put modificar de lo que vamos a recibir sera una url con el mov concreto
// 54 etc y en el body el json con los datos a modificar
//p ejemplo mov buenos aires con el importe tal cambiar a otro
//sustituir un json por el que viene, el mov 1 por el otro
//

app.put('/v2/movimientos/:id',function(req,res) {
     var cambios = req.body
     var actual = movimientosJSON[req.params.id-1]
     if (cambios.importe != undefined ) {
       actual.importe = cambios.importe
     }
     if (actual.ciudad !=undefined) {
       actual.ciudad = cambios.ciudad
     }
     //movimientosJSON[req.params.id-1] = cambios
     res.send("Cambios realizados")
})

app.delete('/v2/movimientos/:id',function(req,res) {
  var actual = movimientosJSON[req.params.id-1]
  movimientosJSON.push({
    "id":movimientosJSON.lenght+1,
    "ciudad":actual.ciudad,
    "importe":actual.importe * (-1),
    "concepto":"Negativo del "+req.params.id
  })
  res.send("Movimiento anulado")
})


//ejercicio con Usuarios

var usuariosJSON = require('./usuarios.json')

app.get('/v1/usuarios/:id',function(req,res) {
  res.send(usuariosJSON [ req.params.id-1 ]);
});

/*app.post('/v1/usuarios/login',function(req,res) {
  var email = req.headers ['email'];
  var password = req.headers ['password']
  res.send("Esperando el login")
});
*/

app.post('/v1/usuarios/login',function(req, res) {
  var email = req.headers['email']
  var password = req.headers ['password']
  var resultados = jsonQuery('[email=' +email+ ']', {data:usuariosJSON})
  if (resultados.value != null && resultados.value.password == password) {
    usuariosJSON[resultados.value.id-1].estado = 'logged'
     res.send('{"login":"OK"}')
   }
   else {
    res.send('{"login":"error"}')
}
})


app.post('/v1/usuarios/logout/:id',function(req,res) {
  var id = req.params.id
  var usuario =usuariosJSON[id-1]
  if (usuario.estado =='logged') {
    usuario.estado ='logout'
    res.send('{"logout":"OK"}')
}
  else {
    res.send('{"logout":"error"}')
}})



//Version 3 de la API conectada a MLab
//var urlMlabRaiz ="https://api.mlab.com/api/1/databases/techumxlas/collections"
//var apiKey = "apiKey=gAEct4cCHzdDKHJnqbdikj2F-JovLHR-"


var urlMlabRaiz = "https://api.mlab.com/api/1/databases/techumx/collections"
var apiKey = "apiKey=50c5ea68e4b0a97d668bc84a"
var clienteMlab = requestJson.createClient(urlMlabRaiz + "?" + apiKey)

app.get('/v3', function(req, res) {
  clienteMlab.get('', function(err, resM, body) {
    var coleccionesUsuario = []
    if (!err) {
      for (var i = 0; i < body.length; i++) {
        if (body[i] != "system.indexes") {
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
        }
      }
      res.send(coleccionesUsuario)
    }
    else {
      res.send(err)
    }
  })
})

app.get('/v3/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v3/usuarios/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  clienteMlab.get('?q={"idusuario":' + req.params.id + '}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})

app.post('/v3/usuarios', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.post('', req.body, function(err, resM, body) {
    res.send(body)
  })
})

app.put('/v3/usuarios/:id', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
  clienteMlab.put('?q={"idusuario": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
    res.send(body)
  })
})


//EJERCICIO TDD

app.get('/v3/movimientos', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v3/movimientos/:idcuenta', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/movimientos")
  clienteMlab.get('?q={"idcuenta":' + req.params.id + '}&' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })
})


//



//V5

//Version 5 de la API conectada a MLab
//var urlMlabRaiz ="https://api.mlab.com/api/1/databases/techumxlas/collections"
//var MYapiKey = "miapiKey=gAEct4cCHzdDKHJnqbdikj2F-JovLHR-"


var MYurlMlabRaiz = "https://api.mlab.com/api/1/databases/techumxlas/collections"
var MYapiKey = "apiKey=gAEct4cCHzdDKHJnqbdikj2F-JovLHR-"
var MYclienteMlab = requestJson.createClient(MYurlMlabRaiz + "?" + MYapiKey)

app.get('/v5', function(req, res) {
  MYclienteMlab.get('', function(err, resM, body) {
    var coleccionesCuentas = []
    if (!err) {
      for (var i = 0; i < body.length; i++) {
        if (body[i] != "system.indexes") {
          coleccionesCuentas.push({"recurso":body[i], "url":"/v5/" + body[i]})
        }
      }
      res.send(coleccionesCuentas)
    }
    else {
      res.send(err)
    }
  })
})

app.get('/v5/cuentas', function(req, res) {
  MYclienteMlab = requestJson.createClient(MYurlMlabRaiz + "/cuentas?" + MYapiKey)
  MYclienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v5/cuentas/:id', function(req, res) {
  MYclienteMlab = requestJson.createClient(MYurlMlabRaiz + "/cuentas")
  MYclienteMlab.get('?f={"movimientos":1}&q={"idCuenta":' + req.params.id + '}&' + MYapiKey,
  function(err, resM, body) {
    res.send(body)
  })
})



app.get('/v5/clientes', function(req, res) {
  MYclienteMlab = requestJson.createClient(MYurlMlabRaiz + "/clientes?" + MYapiKey)
  MYclienteMlab.get('', function(err, resM, body) {
    res.send(body)
  })
})

app.get('/v5/clientes/:id', function(req, res) {
  MYclienteMlab = requestJson.createClient(MYurlMlabRaiz + "/clientes")
  MYclienteMlab.get('?q={"idusuario":' + req.params.id + '}&' + MYapiKey,
  function(err, resM, body) {
    res.send(body)
  })
})

app.post('/v5/clientes', function(req, res) {
  MYclienteMlab = requestJson.createClient(MYurlMlabRaiz + "/clientes?" + MYapiKey)
  var cuerpo= req.body;
  cuerpo.estado=false;
  MYclienteMlab.post('', cuerpo, function(err, resM, body) {
    res.send(body)
  })
})


//V5




app.listen(3000)
console.log("Escuchando en el puerto 3000")
