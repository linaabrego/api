var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server')

var should = chai.should()


chai.use(chaiHttp) // Configurar Chai con el módulo http

describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.com.mx')
        .get('/')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  })
})

describe('Tests de API usuarios', () => {
  it('Raíz de la API contesta', (done) => {
    chai.request('http://localhost:3000')
        .get('/v3')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  })
  it('Raíz de la API funciona', (done) => {
    chai.request('http://localhost:3000')
        .get('/v3')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          res.body.should.be.a('array')
          done()
        })
  })
  it('Raíz de la API devuelve 2 colecciones', (done) => {
    chai.request('http://localhost:3000')
        .get('/v3')
        .end((err, res) => {
          //console.log(res.body)
          res.should.have.status(200)
          res.body.should.be.a('array')
          res.body.length.should.be.eql(2)
          done()
        })
  })
  it('Raíz de API devuelve los objetos correctos', (done) => {
    chai.request('http://localhost:3000')
        .get('/v3')
        .end((err, res) => {
          //console.log(res.body)
          res.should.have.status(200)
          res.body.should.be.a('array')
          res.body.length.should.be.eql(2)
          for (var i = 0; i < res.body.length; i++) {
            res.body[i].should.have.property('recurso')
            res.body[i].should.have.property('url')
          }
          done()
        })
  })
})




describe('Tests de API movimientos', () => {
  it('Raíz de la API movimientos contesta', (done) => {
    chai.request('http://localhost:3000')
        .get('/v3/movimientos')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          res.body.should.be.a('array')
          done()
        })
  })
})


describe('Tests de API movimientos id', () => {
  it('Raíz de la API movimientos id contesta', (done) => {
    chai.request('http://localhost:3000')
        .get('/v3/movimientos/:idcuenta')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          res.body.should.be.a('array')
          done()
        })
  })
})
